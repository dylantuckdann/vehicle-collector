// ==UserScript==
// @name         Phone Search
// @namespace
// @version      0.8
// @description  Craigslist Phone Search
// @author       erynn & Julia :3
// @match        http://*.craigslist.org/*
// @include     *.craigslist.org/*/cto*
// @include     *craigslist.org/cto/*
// @include     https://www.carfaxonline.com/cfm/*
// @require      http://code.jquery.com/jquery-latest.js
// @require      https://gist.github.com/raw/2625891/waitForKeyElements.js
// @grant        none
// ==/UserScript==
     var s = window.location.href;
     s = s.substring(0, s.indexOf('.'));
 $.ajax({
    url:
     s + '.craigslist.org/',
        type:'GET',
        success: function(data){
           var baselocation = $(data).find('h2').html();
           $('.pagelocation').append("<h3 class = 'top-location'>"+baselocation+"</h3>");
           $( ".wikilink" ).remove();
           $(".top-location").css({"text-transform":"capitalize", "font-weight":"normal"});
        }
});
$("img").css({"max-width": "74%", "max-height": "74%"});

$(".arrow").click(function(){
$("img").css({"max-width": "74%", "max-height": "74%"});
$("#thumbs").css({"width":"1000em"});
});
            $(".replay-flap").css({"width": "35%"});

$(".postinginfo").css({"display" : "flex" , "margin":"0", "padding-top":"5px"});
$(".reply_button").css({"float" : "none"});
var $time = $("time");
var $bubbles = $('p.attrgroup, .mapAndAttrs').css({'display':'block','width':'100%','margin':'0'}).insertAfter('#thumbs');
$('.dateReplyBar').find('.postinginfo, .flags, .prevnext, #printme').remove();
$("p").css({"opacity": "100"});
$(".posting .dateReplyBar").css({"margin-top" : "0em"});
$("time").css({"cursor":"text", "border-bottom" : "0px"});
$('.postinginfo').css({ "margin-left": "1.3em"});
var $location = $('<div class="pagelocation"></div>').text(window.location);
$('.body').prepend($location);
$('.dateReplyBar').append($('.postinginfos').html());
$('.mapbox, .tsb, .bchead, .postinginfos, .notices, footer , .bestof-link, sup, .screen-reader-text,  .banish, .icon-star').remove();
$('#oneprice').remove();
$('#nav').remove();
$('#bottombracket').css({'top':'0px'});
$('span').css({'background-color':'#fff', "border":"0px", "padding":"0px"});
$('#cipcontent').css({'padding-left':'0px'});
$("<br><br>").insertBefore(".attrgroup");
$(".email-friend").remove();

function returnPhoneNumber() {
    var copyhref = "https://www.google.com/#&q=";
    rawPhone = $(".reply-tel-number").text();
    var originalText = rawPhone.replace('☎', '').replace(/\s/g, '');
    rawPhone = rawPhone.replace(/[\W_]+/g, ' ').replace(/\s/g, '');


        var formattext = rawPhone;
        formattext = formattext.replace(/(.{3})(.{3})/,'$1-$2-');
        formattext = formattext.replace(/"/g, "").replace(/'/g, "").replace(/\(|\)/g, "");
    
    var googleLink = $("<a class = 'googlePhone'></a>")
                .text('Google Phone Number ')
                .attr({'href': copyhref,'target': 'newwindow'})
                .css({'font-size': '16px','padding-bottom':' 10px', 'padding-left':'5px'});
       if($('#searchapp').length == 1){
            googleLink.insertAfter(".googleLink");
        } else {
            googleLink.insertAfter(".pagelocation");
        }
    $(".googlePhone").attr("href" , 'https://www.google.com/search?q='+ '\"'  + formattext + '"' +' OR "' + rawPhone  + '"' +' OR "' + originalText + '"' + ' %20site:ebay.com%20OR%20site:publicrecords.directory%20OR%20site:craigslist.org%20OR%20site:fyiauto.com%20OR%20%22vehicle%20identification%20number%22%20OR%20VIN%20OR%20auto%20OR%20mile%20OR%20vehicle%20OR%20dealer%20OR%20repair%20OR%20motor%20OR%20RV%20OR%20car%20OR%20truck%20OR%20salvage%20OR%20rebuilt%20OR%20transmission%20OR%20gas%20OR%20sell%20OR%20drive');

}

formatReply = function(reply_options) {
    var $replyOptions = $(reply_options),
        $body = $replyOptions.closest('body'),
        $replyButton = $body.find('.reply_button');
    $(".reply-flap").css({"width": "35%"});
    $replyButton.replaceWith($replyOptions);
    $replyOptions.insertAfter($body.find('.dateReplyBar'));
    $body.find('#webmailinks').remove();
    returnPhoneNumber();
};
$(".postinginfo").css({ "margin" : "0px 0px 0px 0em"});
var delay = Math.ceil(4000*Math.random())+1000;
waitForKeyElements('.reply-flap', formatReply);
setTimeout(function(){$('.reply_button').trigger('nothing'); },delay);