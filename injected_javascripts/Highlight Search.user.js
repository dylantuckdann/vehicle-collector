// ==UserScript==
// @name         Highlight Search
// @namespace    http://chris-a.rocks/
// @version      0.1
// @description  Don't highlight too much!
// @author       Chris Ariagno /u/theguywhocodes
// @include     *://*.craigslist.org/*/cto*
// @include     *://*.craigslist.org/cto*
// @version     1.6.1
// @match        *://*.craigslist.org/cto*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    var copyhref = "https://www.google.com/#&q=" + getSelectionText() + getRawSelectionText() + '\" OR ' + 'site:ebay.com OR site:publicrecords.directory OR site:craigslist.org OR site:fyiauto.com OR "vehicle identification number" OR VIN OR auto OR mile OR vehicle OR dealer OR repair OR motor OR RV OR car OR truck OR salvage OR rebuilt OR transmission OR gas OR sell OR drive';
    var finalcopy = $("<a class = 'googleLink'></a>")
                .text('Highlighted Search ')
                .attr({'href': copyhref,'target': 'newwindow'})
                .css({'font-size': '16px','padding-bottom':' 10px', 'padding-left':'5px'});
    if($('#searchapp').length == 1){ 
        finalcopy.insertAfter("#searchapp"); 
    } else { 
        finalcopy.insertBefore(".pagelocation"); 
    }
     function getSelectionText() {
        var text = "";
        if (window.getSelection) {
            text = window.getSelection().toString();
        } else if (document.selection && document.selection.type != "Control") {
            text = document.selection.createRange().text;
        }
         var query;
        query = text.replace(/ /g, '');
        query = query.replace(")", '');
        query = query.replace("(", '');
        query = query.replace(/_/g,"");
        query = query.replace(/-/g,"");
        query = query.replace(/one/g,"1");
        query = query.replace(/two/g,"2");
        query = query.replace(/three/g,"3");
        query = query.replace(/four/g,"4");
        query = query.replace(/five/g,"5");
        query = query.replace(/six/g,"6");
        query = query.replace(/seven/g,"7");
        query = query.replace(/eight/g,"8");
        query = query.replace(/nine/g,"9");
        query = query.replace(/zero/g,"0");

        return query;
    }
 function getRawSelectionText() {
        var text = "";
        if (window.getSelection) {
            text = window.getSelection().toString();
        } else if (document.selection && document.selection.type != "Control") {
            text = document.selection.createRange().text;
        }
        return text;
    }
 function getFormattedText() {
        var formattext = getSelectionText();
        formattext = formattext.replace(/(.{3})(.{3})/,'$1-$2-');
        formattext= formattext.replace(/"/g, "").replace(/'/g, "").replace(/\(|\)/g, "");
        return formattext;
    }
       $(".body").mouseup(function (e){
          $(".googleLink").attr("href" , 'https://www.google.ca/search?q='+ '\"'  + getSelectionText() + '"' +' OR "' + getRawSelectionText()  + '"' +' OR "' + getFormattedText() + '"' + ' %20site:ebay.com%20OR%20site:publicrecords.directory%20OR%20site:craigslist.org%20OR%20site:fyiauto.com%20OR%20%22vehicle%20identification%20number%22%20OR%20VIN%20OR%20auto%20OR%20mile%20OR%20vehicle%20OR%20dealer%20OR%20repair%20OR%20motor%20OR%20RV%20OR%20car%20OR%20truck%20OR%20salvage%20OR%20rebuilt%20OR%20transmission%20OR%20gas%20OR%20sell%20OR%20drive');
       });



})();