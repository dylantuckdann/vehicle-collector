// ==UserScript==
// @name         Global Variagles
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       Ya boi @cmoneyphd
// @match        *://www.autotrader.com/cars-for-sale/vehicledetails*
// @include     *://*.craigslist.org/*/cto*
// @include     *://*.craigslist.org/cto*
// @include     *://*.craigslist.org/*
// @include     *.craigslist.org/*
// ==/UserScript==

(function() {
    'use strict';
var positive;
unsafeWindow.positive = [ "negotiable","leather","lthr","lather","lether","leathar","lethar","clean title","clean history","-Leather",
                 "clean car history","clean vin","clean carfax","one owner","SH-AWD","(Super Handling All Wheel Drive)","Super Handling All Wheel Drive","Super-Handling All Wheel Drive",
                 "1 owner","one owner","sunroof","moonroof","Z71","texas edition","a w d","4 w d","4-wheel drive","air ride","airride","air suspension",
                 "panoramic","panorama","convertable","nav","navigation","hard top","4 Wheel Drive","20 inch rims","20 inch wheels","TRD",
                 "OBO","or best offer","gps","camera","dvd","Quattro","4matic","4-matic","Bighorn","backup","3rd row","third row","rear bench",
                 "entertainment","tv","television","pkg","flex fuel","Moon Roofs","sun roofs","moonroofs","sunroofs","bucket seats","rear captain","rear bucket",
                 "pack","package","turbo","chrome","back up camera","backup camera","back up cam","backup cam","PANAORAMIC","All wheel dr","All wheel drv","4 wheel drv","four wheel drv","4 wheel dr","four wheel dr",
                 "allwheel","all-wheel","camera","power drivers seat","power tail","power lift gate","power liftgate","power tailgate","auto tailgate","remote tailgate","remote lift","auto lift","auto liftgate","remote liftgate",
                "Power Lift","sky slider","skyslider","skyview","REAR DVD","rear tv","rear ent","rear entertainment","entertainment system","entertainment sys","RES","r.e.s.",
                 "hardtop","hard top","4wd","4x4","4 wheel","big horn","rear camera","Back up cam","Back up camera","Sunroof/Moonroof","moonroof/Sunroof",
                         "Moon Roff","ASAP",
                 "four wheel","awd","All wheel drive","leatherette","leatherett","leatheret","/nav","heated and","cooled","heated leather","cooled leather","heated cloth",
                 "heated seat","heated seats","Heated leather","cooled leather","heat seats","airconditioned seats","air conditioned seats","air-conditioned seats","a/c seats","ac seats","cool seats","cooled seats","power seat","power seats","no accidents","Moon Roof","sun roof",
                "NAVAGATION","LEATER","4 x 4","Heated/Cooled","Cooled/Heated","-Navigation","-sunroof","navi","Seat Heater","Seat Heaters","seat heat","seat cooler",
                 "-Heated Seats","w/navigation","w/nav","w/navi","Heated driver","Heated passenger","-Moon Roof","-nav","-gps","-moonroof","-sunroof",
                "heated front seat","heated front seats","cooling seats","cooling seat","heating seats","heating seat","cooled front seats","cooled back seats","heated rear seat","heated rear seats",
                "no wreck","no wrecks","no accident","no accidents" ];
var negative;
    unsafeWindow.negative = [ "smokes","smoke","smoking","smoked","armada","quest","flex","aspen","veloster","5 series","defects","defect","Rebuild","ex-salvage","exsalvage","exsalvaged","salvage","salvaged",
                 "infiniti","monte carlo","avenger","impala","versa","stains","stained","stain","local only","local buyers","locally only","sell local","sell locally",
                "mitsubishi","sebring","tiburon","durango","e250","scratches","dings","ding","scratch","scratched","mark","marked","marks",
                 "won't start","doesn't start","e350","rx8","miata","fiat","mazda6","pontiac","New engine","not working","won't work","doesn't work","dealers","body work","needs","needing",
                 "magnum","accent","rio","crossfire","pt cruiser","pacifica","minor","blemishes","blemish","blemished","rusted","rips","tears","as is","as-is",
                 "rondo","chrysler 200","saab","rendezvous","lexus gs","sentra","supercharger","supercharged","super charge","super charged","super charger","tuner","chip","chipped",
                 "mx5","accord lx","grand marquis","solara","non-negotiable","rough country","no dealers","no dealer","if you are a dealer","burns","burn","burnt",
                 "not negotiable","custom rims","nitro","nos","injected","firm","cracked","crack","cracks","cracking","aftermarket","programmer","modifications",
                 "noticeable","collision","impact","rear-ended","rearended","damage","damaged","damaging","local only","locals only","only local","be local","local sale only","local buyer only","local buyers only",
                 "rear ended","blemish","barely","wrapped","flaw","loose","mod","incident","incidents","branded","title brand","New transmission",
                 "bump","hole","burn","rip","tear","puncture","flat","dent","problems","problem","issue","issues","Tune","tuning","except","except","except","not including","other than","excluding","aside from",
                 "accident","wreck","scratch","scrape","break","bust","broken","spill","spilt","splash","wet","scratching","fading","faded","fade","new title",
                 "rebuilt","salvaged","deploy","lowball","low-ball","repair","faulty","chips","h3","h3x","does not","leveling kit","Hablo español","ingles","estoy",
                "gracias"," vehículo ","de"," llamada ","automática"," grande ","es ","voy ","en ","coche"," por"," favor"," ayudarle"," ","nombre "," kilometraje ","número ","accidentes"," millas"," rápida ","venta",
                 "fix","fixed","rust","hail","leak","torn","bent","stain","replace","replaced","Lowering","program","programming","programmed",
                 "scuff", "scuffs ","problem","bad","horrible","stink","stank","stunk","dents","worn","fender bender","bender","new tranny","new transmission",
                 "lift","lifted","lowered","new york","nyc","new jersey","Rock Krawler","wear","aftermarket","tree","leaking","leaked","leaks","leak",
                 "miami","lauderdale","doesn't work","stopped working","flood","water","leak","wet","logged","rot","rotten","hauled","towed","to tow","to haul","towing","hauling",
                 "isn't working","slight","modified","tuned","chipped","second motor","new motor","installed motor","fresh motor","have added",
                             "VA DLR","VADLR","fleet","rental",
                "noise","vibrates","pulls","vibration","noisey","humm","tick","ticks","hum","a sound","some sounds","some sound","fixing","fixed","fix",
                 "light is on","engine light","check light","light on","RUSTING","bubbles","bubbling","bubbled","bubble","Exselentes","condisione",
                  "chip","burst","modification","vibrate","cosmetic","wrecked" ];
var neutral;
    unsafeWindow.neutral =  [ "transmission","tread","automatic","manual","2wd","rwd","fwd","small","alloy",
                 "4x2","2 wheel","two wheel","seat","kelly","paperwork","leveling","level","leveled","Newer",
                 "v8","v6","v10","v4","v5","custom","sound system","good","great","auto","trans",
                 "rims","excellent","condition","amazing","roof","cruise","remote","warranty","plus","factory",
                 "push","bucket","amp","liftgate","lift gate","liter","kit","rim","tires","Windows",
                "wheels","lift gate","17 inch","18 inch","19 inch","carpet","stock","never","power","powered","no modifications","zero modifications","no mods","zero mods",
                 "20 inch","21 inch","22 inch","23 inch","24 inch","17\"","18\"","do not",
                 "19\"","20\"","21\"","22\"","23\"","24\"","impact","replaced","replace","replacing",
                 "exhaust","locks","window","tinted","tint","never been","power windows","no bumps","non smoker",
                 "seat","running board","runningboard","extra","mile","miles","milages","mileage","non smoking","no smoking","non-smoker","non-smokers",
                 "kbb","loaded","appointment","maintenance","dealership","bluetooth","Hard","soft top","soft",
                 "dealer","luxury","reasonable","asap","lease","finance","pay","aluminum",
                 "lien","loan","bank","very fair","old","dog","pet","not had any problems","no problems",
                 "animal","trim","cloth","smell","airbag","inch",
                 "title in hand","in hand","hemi","must sell","personal", "non-smoking","no dents","no scrapes","no issues","rust-free",
                 "negotiate","price","coupe","dual","towing","hatchback","wagon","calls after",
                 "bus","sedan","truck","suv","trade","garage","reduced","stereo",
                 "five-speed","six-speed","fivespeed","liter","cylinder","tint","row","Running Boards","runningboard","running board","interior","certified",
                 "adult","carfax","cheap","title","Power:","wheels:",
                 "nada","blue book","smoke","pet","upgrade","subs","automatic","neutral","manual","CVT",
                 "subwoofer","amplifier","after market","installed","garaged","garage",
                 "condition","contact","via","call","cell","telephone","phone",
                 "evening","morning","anytime","reach","email","text","number",
                 "photo","evidence","proof","photograph",
                 "picture","more info","hybrid","electric","customer",
                 "best offer","current offer","top offer","declined","decline",
                 "reject","refuse","owe","cash","money","$","no" ];
var personal;
    unsafeWindow.personal =    [ "my wife","my kids","college","my husband","we","I'm","our","I'll","had this","my","wife","son","father","mother","daughter","college","student","elderly","Owned by","seniors","senior",
                  "teenager","teenage","daughter","son","college","father","mother","grandfather","grandson","grandmother","granddaughter","private seller","private owner","original owner","I was","daily driver",
                   "grandchildren","grandson","granddaughter","grand daughter","grandson's","have owned","inheritance","inherited","baby","bought it","in laws","inlaws","in-laws","in law","inlaw","in-law",
                   "us","selling because","selling due","selling cus","sellin because","sellin due","sellin cus","retired","moving","retirement","retiring","army","over seas","overseas","relocating","relocated","relocate","new job",
                                "I am the","friends","friend","children","kids","wife's","because I","selling because","moving","relocating","relocation",
                   "she","we","us","moving","our","I've","we've","original owner","original owners","mom","dad","grandpa","mom's","dad's","grandpa's","grandma's","wife's","dad's","brother's","sister's","daughter's","son's","purchased",
                 "death","family","moving","I have","I may","we've owned","she's owned","he's owned","I owned","I've had","I've owned","by my","for a neighbor","for a friend","for my","passed away","divorce","divorcing","divorced","her","husbands","husband",  ];
var names;
    unsafeWindow.names =    [ "honda", "toyota", "ford","XLT","Escape","Chevy","Silverado","1500","2500","Edge","lariat","lariet","Latitude","Latitude ",
                 "LT","LTZ","Crew Cab","Extended Cab","Dodge","Ram","quad cab","2006","2007",
                 "2008","2009","2010","2011","2012","2013","2014","2015","2016","fx4","4dr","4 door","4door","four door","4dr,",
                 "Rav4","Jeep","Compass","Commander","Liberty","Wrangler","Unlimited","Limited","ltd","Sahara","Rubicon","Patriot",
                 "Acura","MDX","technology","RDX","xl","xlt","f150","f250","f350","f-150","f-250","f-350" ];
var brands;
    unsafeWindow.brands =   [ "Acura", "Audi", "Buick", "Cadillac", "Chevrolet", "Chevy", "Chrysler", "Dodge", "Ford", "GMC","cyl","2.5l","3.2l","RL","Overland",
                "Hybrid","Touring","4 door",
                 "Honda", "Hummer", "Hyundai", "Jeep", "Kia", "Land Rover", "Landrover", "Lexus", "Lincoln","vin#","3.5l","murano","Tahoe","LS","SL","MINI","COOPER","S","COUNTRYMAN","SR5",
                 "Mazda", "Mercedes-Benz", "Mercedes", "Mercury", "Nissan", "Scion", "Subaru", "Toyota","vin","A5","C300","C250","FJ Cruiser","Laramie","Explorer","Limited","Tacoma","crew cab","crew",
                 "Volkswagen", "VW", "Volvo", "honda", "toyota", "ford","XLT","Escape","Chevy","Silverado","1500","2500","Edge","lariat","lariet","SRX","GRAND CHEROKEE","cherokee","cab",
                 "LT","LTZ","Crew Cab","Extended Cab","Dodge","Ram","quad cab","2006","2007","CRV","EX","CR-V","LX","EX-L","EXL","SLE","all terrain","3.6","3.6L","sport","Ridgeline","hemi","3.5L","Outback","Legacy",
                 "2008","2009","2010","2011","2012","2013","2014","2015","2016","fx4","Yukon","Denali","Highlander","Grand Touring","CX-9","SE","7.3","2500hd","1500hd","3500hd","frontier","Impreza","SEL",
                 "Rav4","Jeep","Compass","Commander","Liberty","Wrangler","Unlimited","Limited","ltd","Sahara","Rubicon","Patriot","Sierra","SLT","5.3","4.8","3.5","2.0","4.0","5.3L","5.7L","4.8L","3.5L","V-6","V6","V8","V-8","Eco Boost","5.7","4.8","6.7","6.7L",
                "Cummings","diesel","deisel","cummins","duramax","Extended Cab","regular cab","crew cab","quad cab","quad","crew","titan","king","double cab","club cab","premium",
                "laredo","RANGER","supercrew","supercab",
                "Platinum","series","edition","4RUNNER","2.5i","2.5x","2.5","3.6R","3.6i","3.6","Special addition","Special eddition","SE","LE","XLE","CE","Base series","Base model",

                 "Acura","MDX","technology","RDX","xl","xlt","f150","f250","f350","f-150","f-250","f-350" ];

})();