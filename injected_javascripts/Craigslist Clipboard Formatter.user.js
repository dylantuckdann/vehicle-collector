// ==UserScript==
// @name         Craigslist Clipboard Formatter
// @version      1.0
// @description  Modifies page for better clipboard conversion
// @author       Julia
// @namespace    http://steamcommunity.com/profiles/76561198080179568
// @include      *://*.craigslist.org/*/cto*
// @include      *://*.craigslist.org/cto*
// @match        *://*.craigslist.org/cto*
// @require      https://gist.github.com/raw/2625891/waitForKeyElements.js
// @grant        none
// ==/UserScript==


$.fn.changeElementType = function(newType) {
    var attrs = {};

    $.each(this[0].attributes, function(idx, attr) {
        attrs[attr.nodeName] = attr.nodeValue;
    });

    this.replaceWith(function() {
        return $("<" + newType + "/>", attrs).append($(this).contents());
    });
};


// function for adding css
function addGlobalStyle(css) {
    var head, style;
    head = document.getElementsByTagName('head')[0];
    if (!head) { return; }
    style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = css;
    head.appendChild(style);
}

// add layout styles
addGlobalStyle('.inventory-cmp-filter .history-comparison-filters { display: flex; flex-direction: row; }');
addGlobalStyle('.reply-flap { padding: 0; border: none; }');
addGlobalStyle('.userbody { list-style-type: none; }');
addGlobalStyle('.posting .postingtitle { margin: 0; }');

function clipboardFormatReplyLinks() {
    // only keep these fields
    var keep = [
        'contact name',
        'text',
        'reply by email'
    ];
    var keepReg = new RegExp(keep.join('|'), 'i');
    $('.reply-flap li').each(function() {
        var $this = $(this);
        var $h1 = $this.find('h1');
        
        if (!$h1.text().match(keepReg)) {
            $this.remove();
        }
    });
    
    $('.reply-flap h1').remove(); // change h1 to bold
    $('.reply-flap li, .reply-flap ul').changeElementType('div'); // change list elements to divs
    $('.reply-flap p').css({
        'text-indent': '0'
    });
    $('.reply-tel-number').text($('.reply-tel-number').text().replace('☎ ', '')); // remove phone icon
    $('<br/>').insertBefore('.reply-flap'); // add a break before posting body
    $('<br/>').insertAfter('.reply-flap'); // add a break before posting body
}

function clipboardFormatterReady() {
    // is not ad page
    if (!$('#postingbody').length) {
        return;
    }
    
    // replace image sources with fullsize version
    $('#thumbs a').each(function() {
        var $a = $(this);
        var $img = $a.find('img');
        var fullsize = $a.attr('href');
        
        $img.attr('src', fullsize);
    });
    $('.posting .postingtitle').css({
        'margin': '0',
        'font-size': '1.2em'
    });
    
    $('.slider-info').remove(); // remove thumbnail info
    $('.slider-back').remove(); // remove thumbnail info
    $('.slider-forward').remove(); // remove thumbnail info
    // $('.gallery').remove(); // remove gallery main
    
    $('.iw.multiimage br').remove(); // remove breaks
    $('#postingbody').css({
        'font-size': '1.1em'
    });
    $('.attrgroup span').css({
        'line-height': 'normal',
        'font-size': '1.1em',
        'margin': 0
    }); // add single breaks after each span
    // $('.attrgroup span'); // add single breaks after each span
    
    $('.attrgroup span').wrap('<div/>');
    
    var $firstp = $('.attrgroup:first');
    var $bubbles = $('.attrgroup div');
    
    $bubbles.appendTo($firstp);
    $('.attrgroup:not(:first)').remove();
    $('#thumbs').insertAfter('#postingbody'); // move images so that it's after avoid-scams
}

waitForKeyElements('.reply-flap h1:first', clipboardFormatReplyLinks); // wait for reply button to be clicked

$(document).ready(clipboardFormatterReady);