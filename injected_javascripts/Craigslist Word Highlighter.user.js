// ==UserScript==
// @name        Craigslist Word Highlighter
// @namespace   http://steamcommunity.com/profiles/76561198080179568/
// @include     *://*.craigslist.org/*/cto*
// @include     *://*.craigslist.org/cto*
// @include     *://*.craigslist.org/*
// @include     *.craigslist.org/*
// @version     1.6.2
// @grant       none
// @require     http://pastebin.com/raw/ESL1xYZ7
// @require      http://code.jquery.com/jquery-latest.js
// @require     https://gist.github.com/raw/2625891/waitForKeyElements.js
// ==/UserScript==
   $(".body").css({"max-width":"800em"});


var words = positive.concat(names, neutral, negative),
    css = ".p { color: #0f6f00; font-weight: bold; }"
        + ".n { color: #bd0000; font-weight: bold; }"
        + ".t { font-weight: bold; }"
        + ".b { color: #0038ae; font-weight: bold; }"
        + ".reply_options { font-size: 0.8em; }",
    style = $('<style type="text/css"></style>').html(css).appendTo('head'),


_ready = function() {
    _formatBody();
};

_formatBody = function(n) {
    var body = n && $(n).closest('body') || $('body');

var posinstance = new Mark(document.querySelector("#postingbody"));
 posinstance.mark(positive, {
  accuracy: {
  	value: "exactly",
    limiters: [".", ",", "!"]
  },
  separateWordSearch: false,
  diacritics: false,
  className: "positive"
});
var neginstance = new Mark(document.querySelector("#postingbody"));
 neginstance.mark(negative, {
  accuracy: {
  	value: "exactly",
    limiters: [".", ",", "!"]
  },
  separateWordSearch: false,
  diacritics: false,
  className: "negative"
});

var instance = new Mark(document.querySelector("#postingbody"));
instance.mark(neutral, {
  accuracy: {
  	value: "exactly",
    limiters: [".", ",", "!"]
  },
  separateWordSearch: false,
  diacritics: false,
  className: "neutral"
});


var instance = new Mark(document.querySelector("#postingbody"));

instance.mark(brands, {
  accuracy: {
  	value: "exactly",
    limiters: [".", ",", "!"]
  },
  separateWordSearch: false,
  diacritics: false,
  className: "brandinsta"
});
var instance = new Mark(document.querySelector("#postingbody"));
instance.mark(personal, {
  accuracy: {
  	value: "exactly",
    limiters: [".", ",", "!"]
  },
  separateWordSearch: false,
  diacritics: false,
  className: "personal"
});

$('.positive').css({'color': '#0f6f00', 'font-weight': 'bold', "background-color":"transparent"});
$('.negative').css({'color': '#bd0000', 'font-weight': 'bold', "background-color":"transparent"});
$('.brandinsta').css({'color': 'blue', 'font-weight': 'bold', "background-color":"transparent"});
$('.personal').css({'color': 'purple', 'font-weight': 'bold', "background-color":"transparent"});
$('.neutral').css({'font-weight':'bold', "background-color":"transparent"});

    body.find('.mapbox, .tsb, footer').remove();

    _checkValue(body);
};

_checkValue = function(body) {
    var posttitle = body.find('.postingtitletext'),
        costRegex = /\$([\d\,]*)/,
        yearRegex = /^\d{4}/,
        milesRegex = /odometer ?: ?(\d*)/,
        vehicleTypeRegex = /type ?: ?(.*)/,
        driveRegex = /drive ?: ?(.*)/,
        transmissionRegex = /transmission ?: ?(.*)/,
        cylinderRegex = /cylinders ?: ?(.*)/,
        vinRegex = /VIN ?: ?(.*)/,
        costMatch = posttitle.text().match(costRegex),
        costString = costMatch && costMatch[1].replace(/,/, ''),
        vehicle = toTitleCase($('.attrgroup span').first().text()),
        cost = costString && parseInt(costString),
        vehicleType,drive,year,miles,transmission,cylinders,make,model,vin;

    // Get the make and model
    for (var i = 0; i < brands.length; i++) {
        var brand = brands[i],
            index = vehicle.indexOf(brand);

        if (index > -1) {
            make = brand;
            model = vehicle.substring(index + make.length + 1, vehicle.length).trim();
            break;
        }
    }

    body.find('.attrgroup > span').each(function() {
        var text = $(this).text(),
            milesMatch = text.match(milesRegex),
            yearMatch = text.match(yearRegex),
            driveMatch = text.match(driveRegex),
            vehicleTypeMatch = text.match(vehicleTypeRegex),
            transmissionMatch = text.match(transmissionRegex),
            cylinderMatch = text.match(cylinderRegex),
            vinMatch = text.match(vinRegex);

        if (yearMatch) {
            year = parseInt(yearMatch[0]);
        } else if (milesMatch) {
            miles = parseInt(milesMatch[1]);
        } else if (driveMatch) {
            drive = driveMatch[1].toLowerCase();
        } else if (vehicleTypeMatch) {
            vehicleType = vehicleTypeMatch[1].toLowerCase();
        } else if (transmissionMatch) {
            transmission = transmissionMatch[1].toLowerCase();
        } else if (cylinderMatch) {
            cylinders = parseInt(cylinderMatch[1].toLowerCase().replace(/[^0-9.]/g, ''));
        } else if (vinMatch) {
            vin = vinMatch[1];
        }
    });

    if (cost && miles) {
        miles = miles + 2000; // Add 200 miles for inaccuracies

        if (miles % 1000 === 0) {
            miles += 100;
        }

        var badMilesYear = year && ((year >= 2014 && miles > 50000) || (year >= 2013 && miles > 59999)
                                 || (year >= 2012 && miles > 70000) || (year >= 2011 && miles > 79999)
                                 || (year >= 2010 && miles > 90000) || (year >= 2006 && miles > 97999)),
            badCostMiles = (cost > 20000 && miles > 59999),
            badYear = year && year <= 2005,
            is2wd = drive && (drive == '2wd' || drive == 'rwd' || drive == 'fwd'),
            isBadType = vehicleType && (vehicleType == 'suv' || vehicleType == 'truck' || vehicleType == 'pickup' || vehicleType == 'offroad'),
            badCostVehicleTypeDrive = (cost > 14999 && isBadType && is2wd),
            negative = (badMilesYear || badCostMiles || badYear || badCostVehicleTypeDrive);

        if (negative) {
            body.find('.postingtitletext').addClass('n');
            body.find('.attrgroup > span').each(function() {
                var text = $(this).text(),
                    yearMatch = text.match(yearRegex),
                    milesMatch = text.match(milesRegex),
                    driveMatch = text.match(driveRegex),
                    vehicleTypeMatch = text.match(vehicleTypeRegex),
                    negative = ((yearMatch && (badYear || badMilesYear))
                             || (milesMatch && (badCostMiles || badMilesYear))
                             || (driveMatch && (badCostVehicleTypeDrive ))
                             || (vehicleTypeMatch && (badCostVehicleTypeDrive)));

                if (negative) {
                    $(this).addClass('n');
                }
            });
        }
    }

    var n = { 'ModelYear': year,
              'Odometer': miles,
              'EngineCylinderCount': cylinders,
              'TransmissionType': transmission && toTitleCase(transmission),
              'Make': make,
              'Model': model,
              'Vin': vin,
              'DriveTrainType': drive && drive.toUpperCase() },
        params = '';
    
    for (var k in n) {
        var val = n[k];
        
        if (val) {
            params += '&' + k + '=' + val;
        }
    }
    
    var href = 'http://www2.vauto.com/Va/Appraisal/Default.aspx?new=true' + params,
        link = $('<a id = "searchapp">Search Appraisal</a> ').attr('href', href).attr('target', '_blank');
    $('.global-header').remove();
    $('.body').prepend(link);
};
_convertPhoneNumber = function(number) {
    return number.replace(/[a-z]/g,function(y){y=y.charCodeAt(0)-91;return y>27?9:y>24?8:y>20?7:~~(y/3);});
};


$(document).ready(function() {
   _ready();
});

$( ".showcontact" ).click(function() {
    setTimeout(_formatBody,1000);
});

function toTitleCase (string) {
    // \u00C0-\u00ff for a happy Latin-1
    return string.toLowerCase().replace(/_/g, ' ').replace(/\b([a-z\u00C0-\u00ff])/g, function (_, initial) {
        return initial.toUpperCase();
    }).replace(/(\s(?:de|a|o|e|da|do|em|ou|[\u00C0-\u00ff]))\b/ig, function (_, match) {
        return match.toLowerCase();
    });
}